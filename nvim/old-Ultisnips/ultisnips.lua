return {
	"SirVer/ultisnips",
	lazy = false,
	config = function()
		-- Til NVim, hvor skal Ultisnips lede efter snippets
		vim.g.UltiSnippetDir = "~/.config/nvim/mysnips"
		vim.g.UltiSnipsSnippetDirectories = {'UltiSnips','mysnips'}

		vim.g.UltiSnipsExpandTrigger = '<tab>'
		vim.g.UltiSnipsJumpForwardTrigger = '<tab>'
		vim.g.UltiSnipsJumpBackwardTrigger = '<s-tab>'
	end,
}
