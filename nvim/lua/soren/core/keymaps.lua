-- Åbner en terminal i samme direktorie som current buffer
vim.keymap.set("n", "<C-Space>", function() return vim.cmd('silent ! alacritty &') end)

-- calls Metapost_compiler_toggle
vim.keymap.set("n", "<leader>mm", function() return Metapost_compiler_toggle() end)
-- åbner Log, hvis du er in en .mp -fil
vim.keymap.set("n", "<leader>ml", function() return vim.cmd('Log') end)
