local metapost_compiler_running = false
local compiler_greeting = "My MetaPost: "

function Metapost_compiler_toggle()
	if vim.fn.expand('%:e') == 'mp' then
		if metapost_compiler_running then
			metapost_compiler_running = false
			print(compiler_greeting .. "MetaPost compiler stopped")
			-- in the future, you should probably delete the autocmd again using: vim.api.nvim_del_autocmd
		else
			vim.api.nvim_create_autocmd("BufWritePost",{
				group = vim.api.nvim_create_augroup("my-metapost-automatic-compiler",{clear = true}),
				pattern = vim.fn.expand('%'),
				callback = function()
					if metapost_compiler_running == true then
						return Metapost_compile()
					end
				end,
			})
			metapost_compiler_running = true
			print(compiler_greeting .. "MetaPost compiler started")
			Metapost_compile()
		end
	else
		print(compiler_greeting .. "This is not a .mp file")
	end
end

local function metapost_print_done()
	print(compiler_greeting .. "Command finished")
end

local function metapost_print_err(_, data)
	if data then
		print(data)
	end
	print(compiler_greeting .. "The mpost command itself has failed - something is terribly wrong!")
end

local function metapost_stdout(_, data)
	if data then
		print(data)
	end
end

function Metapost_compile()
	if vim.fn.expand('%:e') == 'mp' then
		-- FIX: Find out how to print the path s.t. Bash accepts it
		local command_dirty = "cd " .. vim.fn.expand('%:p:h') .. " && mpost " .. vim.fn.expand('%:t')
		-- replace "(" and ")" by "\(" and "\)"
		local command_clean = string.gsub(string.gsub(command_dirty,"%(","\\%("),"%)","\\%)")
		vim.fn.jobstart(command_clean ,{
			on_exit = metapost_print_done,
			on_stdout = metapost_stdout,
			on_stderr = metapost_print_err,
		})
		vim.cmd("redraw!")
		print(compiler_greeting .. "Compilation Complete")
	else
		print(compiler_greeting .. "This is not a .mp file")
	end
end

function Toggle_Log_Window()
	vim.api.nvim_open_win(0,false,{
		height=10,
	})
end
