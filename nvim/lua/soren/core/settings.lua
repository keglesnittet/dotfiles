-- tab er 4 lang
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4

vim.o.fileencodings = 'uft-8'

-- linjenummerering
vim.o.number = true
vim.o.relativenumber = true

-- auto.complete med filer osv.
vim.o.nativemenu = true

--  Gør så cursoren ikke er i toppen eller
--  bunden af skærmen det meste af tiden.
vim.o.scrolloff = 4

-- splitter til højre og under i stedet for venstre og top
vim.o.splitbelow = true
vim.o.splitright = true

-- use the systemet clipboard
vim.opt.clipboard:append { 'unnamed', 'unnamedplus' }

--  gør søgninger case-insensitive
vim.o.ignorecase = true

--  markerer når du søger
vim.o.hlsearch = true

--  søger efter hvert symbol
vim.o.incsearch = true

--  gør den case-insensitive (abc vil matche med Abc, men Abc vil ikke matche med abc)
vim.o.smartcase = true

-- sætter nowrap (på alle filer)
vim.o.wrap = false

-- fjerner de der super irriterende errors i dokumentet, når man ikke skriver
-- Jeg tror de kaldes "lints" eller "linting"
-- Gør kun følgende på .tex filer (men hvis du skifter til en anden fil tror jeg
-- ikke den starter dem igen
if vim.fn.expand('%:e') == 'tex' then
	vim.o.signcolumn = "no"
end

if vim.fn.expand('%:e') == 'cls' or vim.fn.expand('%:e') == 'bib' then
	vim.lsp.handlers["textDocument/publishDiagnostics"] = function() end
end

-- Add border to lsp diagnostic dialogs
local border = "single"
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
  vim.lsp.handlers.hover, {
    border = border,
	virtual_text = false,
  }
)
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
  vim.lsp.handlers.signature_help, {
    border = border
  }
)
vim.diagnostic.config{
  float={border=border}
}
