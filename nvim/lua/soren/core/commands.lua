-- en kommando til at vise log-filen for dokumentet, man er i
-- TODO: maybe check if the log exists first.
vim.api.nvim_create_user_command('Log',
	function()
		vim.cmd("10sp %:p:r.log")
	end,
	{})

-- en kommando der tæller ord og anslag i en .tex fil
vim.api.nvim_create_user_command('Ord',
	function()
		if vim.fn.expand('%:e') == 'tex' then
			vim.cmd("w")
			local ord	 = tonumber(vim.fn.system('texcount -inc -total -template={word} ' .. vim.fn.expand('%:p')))
			local anslag = tonumber(vim.fn.system('texcount -char -inc -total -template={word} ' .. vim.fn.expand('%:p')))
			local inline = vim.fn.system('texcount -inc -total -template={inline} ' .. vim.fn.expand('%:p'))
			local total  = ord + anslag - 1
			local besked = "Ord: ".. tostring(ord) .."    Anslag uden mellemrum: ".. tostring(anslag).."    Anslag med mellemrum (Ca.): ".. tostring(total) .."    Antal inline: "..inline
			print(besked)
		else
			print("Du er ikke i en .tex fil")
		end
	end,
	{})
