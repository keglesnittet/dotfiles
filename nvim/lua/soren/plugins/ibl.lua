return {
	"lukas-reineke/indent-blankline.nvim",
	lazy = false,
	config = function()
		require("ibl").setup {
			exclude = {
				filetypes = {"tex"}
			}
		}
		vim.g.indent_blankline_filetype_exclude = {'lspinfo', 'packer', 'checkhealth', 'help', 'man', 'tex', ''}
	end,
}
