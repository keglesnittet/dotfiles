return {
	"nvim-tree/nvim-tree.lua",
	version = "*",
	lazy = false,
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	config = function()
		require("nvim-tree").setup()
		-- Kører NvimTreeToggle, når du trykker <leader>t
		vim.keymap.set("n", "<leader>t", vim.cmd.NvimTreeToggle)
	end,
}
