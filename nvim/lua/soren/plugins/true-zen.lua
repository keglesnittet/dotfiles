return {
	"Pocco81/true-zen.nvim",
	config = function()
		require("true-zen").setup {
			modes = { -- configurations per mode
				ataraxis = {
					minimum_writing_area = {
						width = 74,
						height = 44,
					},
					quit_untoggles = true, -- type :q or :qa to quit Ataraxis mode
					padding = { -- padding windows
					left = 52,
					right = 52,
					top = 0,
					bottom = 0,
					},
				},
			},
			integrations = {
				lualine = false -- hide nvim-lualine (ataraxis)
			},
		}
		vim.keymap.set("n", "<leader>z", vim.cmd.TZAtaraxis)
	end,
}
