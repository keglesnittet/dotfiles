return {
	"folke/todo-comments.nvim",
	config = function()
		require('todo-comments').setup{
			signs = false, -- do not print signs in the code indicating todos
			keywords = {
				LAST  = { icon = "", color = "last" },
			},
			colors = {
				last  = { "#a89984" },
			},
		}
		-- keymaps til at hoppe mellem todos (med "[t" og "]t")
		vim.keymap.set("n", "]t", function() require("todo-comments").jump_next() end, { desc = "Next todo comment" })
		vim.keymap.set("n", "[t", function() require("todo-comments").jump_prev() end, { desc = "Previous todo comment" })
	end,
}
