return {
	"hrsh7th/nvim-cmp",
	-- load cmp on InsertEnter
	event = "InsertEnter",
	-- these dependencies will only be loaded when cmp loads
	-- dependencies are always lazy-loaded unless specified otherwise
	dependencies = {
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-omni",
		"hrsh7th/cmp-path",
		"quangnguyen30192/cmp-nvim-ultisnips",
	},
	config = function()
		local cmp = require'cmp'

		cmp.setup({
			snippet = {
				expand = function(args)
					vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
				end,
			},
			view = {
				entries = { name = 'native', selection_order = 'near_cursor' } -- can be "custom", "wildmenu" or "native"
			},
			window = {
				completion = cmp.config.window.bordered(),
				documentation = cmp.config.window.bordered(),
			},
			mapping = cmp.mapping.preset.insert({
				['<C-j>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 'c' }), -- disse to linjer har du selv tilføjet
				['<C-k>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 'c' }), -- de gør så du kan scrolle i popup med '<C-j>' og '<C-k>'
				['<C-b>'] = cmp.mapping.scroll_docs(-4),
				['<C-f>'] = cmp.mapping.scroll_docs(4),
				['<C-Space>'] = cmp.mapping.complete(),
				['<C-e>'] = cmp.mapping.abort(),
				['<CR>'] = cmp.mapping.confirm({ select = false }), -- Set `select` to `false` to only confirm explicitly selected items.
			}),
			sources = cmp.config.sources({
				{ name = 'nvim_lsp' },
				{ name = 'ultisnips' },
				{ name = 'path' },
				{ name = 'buffer' }, -- Autocompleter ordene i den fil, du skriver i
				--      { name = 'omni' },
			}),
			completion = {
				keyword_length = 3
			}
		})

		-- Set configuration for specific filetype.
		cmp.setup.filetype('gitcommit', {
			sources = cmp.config.sources({
				{ name = 'buffer' },-- if installed you probably want to also use 'git-cmp'
			})
		})

		-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
		--cmp.setup.cmdline(':', {
		--  mapping = cmp.mapping.preset.cmdline(),
		--  sources = cmp.config.sources({
		--    { name = 'path' }
		--  }, {
		--    { name = 'cmdline' }
		--  })
		--})
	end,
}
