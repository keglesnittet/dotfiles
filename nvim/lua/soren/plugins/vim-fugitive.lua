return {
	"tpope/vim-fugitive",
	config = function()
		vim.keymap.set("n", "<leader>gs", function() return vim.cmd('Git status') end)
		vim.keymap.set("n", "<leader>ga", function() return vim.cmd('Git add %') end)
		vim.keymap.set("n", "<leader>gc", function() return vim.cmd('Git commit') end)
	end,
}
