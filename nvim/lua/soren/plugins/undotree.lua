return {
	"mbbill/undotree",
	lazy = false,
	config = function()
		vim.o.swapfile = false
		vim.o.backup = false
		vim.o.undofile = true
		vim.o.undodir = '/home/soren/.cache/nvim/undodir'

		-- Kører UndoTreeToggle, når du trykker <leader>u
		vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
	end,
}
