return {
	"lervag/vimtex",
	config = function()
		vim.g.vimtex_compiler_progname = '/usr/bin/nvr'
		-- sætter zathura til pdf-viseren
		vim.g.vimtex_view_method = 'zathura_simple'
		vim.g.vimtex_view_general_viewer = 'zathura'
		-- gør så det der Quickfix-vindue ikke popper op
		vim.g.vimtex_quickfix_mode = 0
		-- fjerner error message fra VimTex, om at Tree-Sitter står for highlighting
		vim.g.vimtex_syntax_enabled = 0
		vim.g.vimtex_syntax_conceal_disable = 1
		-- Enable folds in vimtex
		-- vim.g.vimtex_fold_enabled = 1
	end,
}
