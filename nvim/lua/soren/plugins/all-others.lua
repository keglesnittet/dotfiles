return {
	-- the colorscheme should be available when starting Neovim
	-- if some code requires a module from an unloaded plugin, it will be automatically loaded.
	-- So for api plugins like devicons, we can always set lazy=true
	{ "nvim-tree/nvim-web-devicons", lazy = true },
	{ "honza/vim-snippets" },
	{
		"numToStr/Comment.nvim",
		config = true,
	},
	-- du har installeret 'ripgrep' og 'fd' kun for at bruge følgende
	{
		"williamboman/mason.nvim",
		config = true,
	},
	{
		"williamboman/mason-lspconfig.nvim",
		config = true,
	},
	{ -- give it a chance! - gives smooth scrolling.
		"karb94/neoscroll.nvim",
		config = true,
	},
	-- consider usig 'wellle/targets.vim'
	{ -- give it a chance! - allows changing surrounding delimiters easily
		'tpope/vim-surround',
	},
}
