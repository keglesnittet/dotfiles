return {
	"L3MON4D3/LuaSnip",
	version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
	build = "make install_jsregexp",
	dependencies = { "saadparwaiz1/cmp_luasnip" },
	config = function ()
		local ls = require("luasnip")
		-- make <Tab> both the LuaSnip expander and jumper
		-- and work as usual if there is nothin to expand or jump to
		vim.keymap.set({ "i", "s" }, "<Tab>", function()
			if ls.expand_or_jumpable() then
				ls.expand_or_jump()
			else
				vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Tab>", true, false, true), "n", false)
			end
		end, { desc = "Snippet next argument", silent = true })

		-- <S-Tab> to jump backwards (if possible)
		vim.keymap.set({"i", "s"}, "<S-Tab>", function() ls.jump(-1) end, {silent = true})

		-- <C-J> and <C-K> to scroll choices in choice nodes
		vim.keymap.set({"i", "s"}, "<C-J>", function()
			if ls.choice_active() then
				ls.change_choice(1)
			end
		end, {silent = true})
		vim.keymap.set({"i", "s"}, "<C-K>", function()
			if ls.choice_active() then
				ls.change_choice(-1)
			end
		end, {silent = true})

		require("luasnip.loaders.from_lua").lazy_load()

		ls.config.set_config({
			history = true,
			enable_autosnippets = true,
			store_selection_keys = "<Tab>",
			updateevents = "TextChanged,TextChangedI",
		})
	end,
}
