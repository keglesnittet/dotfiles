-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- sætter leader key til mellemrum
vim.g.mapleader = " "

vim.cmd('filetype plugin indent on')

require("soren.lazy")

vim.o.completeopt = 'menu,menuone,noselect'

-- gør så filerne med extensionen .md behandles som markdown filer
vim.cmd('autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc')
vim.cmd('autocmd BufNewFile,BufFilePre,BufRead *.md set syntax=markdown.pandoc')

require('soren.core.metapost')
require('soren.core.commands')
require('soren.core.settings')
require('soren.core.keymaps')
