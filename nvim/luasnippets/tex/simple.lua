local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

return {
	-- these are the simple non-automatic snippets for TeX files.

	s( "wlog", {
		t"uden tab af generalitet "
	}),

	s( "iff", {
		t"if and only if "
	}),

	s( "hviss", {
		t"hvis og kun hvis "
	}),

	s( "hviss", {
		t"hvis og kun hvis "
	}),

	s( "mm" , {
		t"mutatis mutandis "
	}),

	s( "af" , {
		t"\\textit{a fortiori} "
	}),

	s( "ses" , {
		t"short exact sequence "
	}),

	s( "les" , {
		t"long exact sequence "
	}),

	s( "xelatex" , {
		t"%!TeX program = xelatex"
	}),

	s( "lualatex" , {
		t"%!TeX program = lualatex"
	}),

	s( "%%" , {
		t"% TODO: ", i(0, "DIN_TODO")
	}),

	s( { trig = "\\a", wordTrig = false} , {
		t"\\alpha "
	}),

	s( { trig = "\\b", wordTrig = false} , {
		t"\\beta "
	}),

	s( { trig = "\\d", wordTrig = false} , {
		t"\\delta "
	}),

	s( { trig = "\\D", wordTrig = false} , {
		t"\\Delta "
	}),

	s( { trig = "\\z", wordTrig = false} , {
		t"\\zeta "
	}),

	s( { trig = "\\s", wordTrig = false} , {
		t"\\sigma "
	}),

	s( { trig = "\\e", wordTrig = false} , {
		t"\\epsilon "
	}),

	s( { trig = "\\g", wordTrig = false} , {
		t"\\gamma "
	}),

	s( { trig = "\\G", wordTrig = false} , {
		t"\\Gamma "
	}),

	s( "up" , {
		t"\\usepackage{", i(0), t"}"
	}),

	s( "oo" , {
		t"\\infty "
	}),

	s( "ø" , {
		t"\\emptyset "
	}),

	s( "|" , {
		t"\\pipe "
	}),

},

{
	-- these are the simple autosnippets.
	s( "**" , {
		t"\\cdot "
	}),

	s( { trig = "::", wordTrig = false } , {
		t"\\colon "
	}),

	s( { trig = ":=", wordTrig = false } , {
		t"\\coloneqq "
	}),

}
