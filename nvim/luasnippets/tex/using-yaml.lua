local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local extras = require("luasnip.extras")
local fmt = require("luasnip.extras.fmt").fmt
local l = require("luasnip.extras").lambda
local dl = extras.dynamic_lambda

-- Summary: When `LS_SELECT_RAW` is populated with a visual selection, the function
-- returns an insert node whose initial text is set to the visual selection.
-- When `LS_SELECT_RAW` is empty, the function simply returns an empty insert node.
local get_visual = function(args, parent)
	if (#parent.snippet.env.LS_SELECT_RAW > 0) then
		return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
	else  -- If LS_SELECT_RAW is empty, return a blank insert node
		return sn(nil, i(1))
	end
end

-- given a path this returns the head/parrent of the path
local function headOfPath(path)
	if string.match(path, "/") then
		return string.reverse(string.sub(string.match(string.reverse(path), "/.*"),2))
	else
		return ""
	end
end

-- returns true if "name" is the path of a file which exists (is readable)
local function file_exists(name)
   local file=io.open(name,"r")
   if file~=nil then io.close(file) return true else return false end
end

local function yaml_exists()
	local path_to_check = vim.fn.expand('%:p')
	while #path_to_check > 0 do
		if file_exists(path_to_check .. "/.info.yml") then
			return true
		end
		path_to_check = headOfPath(path_to_check)
	end
	return false
end

local function yaml_path()
	if yaml_exists() == true then
		local path_to_check = vim.fn.expand('%:p')
		while #path_to_check > 0 do
			if file_exists(path_to_check .. "/.info.yml") then
				return path_to_check .. "/.info.yml"
			end
			path_to_check = headOfPath(path_to_check)
		end
	end
end

local function get_info()
	if yaml_exists() then
		local yaml = require('lyaml')
		local info_file = io.open(yaml_path(), "r")
		local content = info_file:read("*all")
		info_file:close()
		return yaml.load(content)
	else
		return nil
	end
end

local info = get_info()
-- if the yaml file exists you can get the value of e.g. the field "sprog" by info.sprog

return {
	-- non-automatic snippets

	s("fullassignment", fmt([[
	\documentclass<sprog>{homework}
	\usepackage{biblatex}
	\addbibresource{KU.bib}<quiver>

	<nummer>
	\author{Søren Skeie\\(nlw315)}
	\title{<titel>}
	\date{\formatdate<dato>}

	\begin{document}
	\maketitle

	\section*{<sec_title>}

	<visual>

	\printbibliography
	\end{document}
	]], {
		sprog = f(function() if info.sprog  == "dansk" then return "[dansk]" else return "" end end),
		quiver = f( function()
				if info.quiver == "yes" then
					return { "", "\\usepackage{quiver}" }
				else
					return ""
				end end),
		nummer = f(function()
			local nummer = info.opgave_nummer
			if nummer ~= "\\textbf{\\alph*)}" and nummer ~= nil then
				return "\\renewcommand{\\nummer}{" .. nummer .. "}"
			else return "" end end),
		titel = d(1, function() return sn(nil, i(1,info.titel)) end),
		dato = f(function() return os.date("{%d}{%m}{%Y}") end),
		sec_title = f(function() if info.sprog == "dansk" then return "Problemer \\& Løsninger" else return "Problems \\& Solutions" end end),
		visual = d(2, get_visual),
	}, { delimiters = "<>" })),

	s( "\\fig", fmt([[
	\begin{figure}[h]\centering
	\caption{<caption>}\label{fig:<label>}
	\includegraphics[width=.8\textwidth,height=.6\textwidth,keepaspectratio]{<fig_path>-<nummer>.mps}
	\end{figure}


	]], {
		caption = i(1,"caption"),
		label = dl(2, l._1:gsub(" ", "_"):lower(), 1),
		fig_path = f(function()
			if yaml_exists() then
				return info.mpost.main .. info.mpost.out .. vim.fn.expand('%:t:r')
			else
				return "Metapost/"
			end
		end),
		nummer = i(3, "nummer"),
	}, { delimiters = "<>" })),

}
