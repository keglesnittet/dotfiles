local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
-- local r = ls.restore_node
local extras = require("luasnip.extras")
local rep = extras.rep
local fmt = require("luasnip.extras.fmt").fmt
local l = require("luasnip.extras").lambda
local dl = extras.dynamic_lambda

-- TODO: in the future use treesitter to get math context

-- Summary: When `LS_SELECT_RAW` is populated with a visual selection, the function
-- returns an insert node whose initial text is set to the visual selection.
-- When `LS_SELECT_RAW` is empty, the function simply returns an empty insert node.
local get_visual = function(args, parent)
	if (#parent.snippet.env.LS_SELECT_RAW > 0) then
		return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
	else  -- If LS_SELECT_RAW is empty, return a blank insert node
		return sn(nil, i(1))
	end
end

return {
	-- non-automatic snippets

	s( "]i" , fmt([[
	\item {}
	]i
	]], {
		i(1),
	})),

	s( '""' , fmt('"{}"', {
		d(1, get_visual),
	})),

	s( "\\beg" , fmt([[
	\begin{<>}
		<>
	\end{<>}
	]], {
		i(1), d(2, get_visual), rep(1),
	}, { delimiters = "<>" })),

	s( "/" , fmt("\\frac{<>}{<>} ", {
		d(1, get_visual), i(2)
	}, { delimiters = "<>" })),

	s( "--" , fmt( "\\emph{<>} " ,{
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( "bf" , fmt( "\\textbf{<>} " ,{
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( "skriv" , fmt([[
	\overskrift
	\hurtigskrivning{<dato>}
	
	\hfill\formatdate<dato_2>
	]] ,{
		dato = f(function() return os.date("%d-%m-%y") end),
		dato_2 = f(function() return os.date("{%d}{%m}{%Y}") end),
	}, { delimiters = "<>" })),

	s( "\\sec" , fmt([[
	\section{<>}
	\label{sec:<>}


	]],{
		i(1, "section"),
		dl(2, l._1:gsub(" ", "_"):lower(), 1),
	}, { delimiters = "<>" })),

	s( "\\ssec" , fmt([[
	\subsection{<>}
	\label{ssec:<>}


	]],{
		i(1, "subsection"),
		dl(2, l._1:gsub(" ", "_"):lower(), 1),
	}, { delimiters = "<>" })),

	s( "\\sssec" , fmt([[
	\subsubsection{<>}
	\label{sssec:<>}


	]],{
		i(1, "subsubsection"),
		dl(2, l._1:gsub(" ", "_"):lower(), 1),
	}, { delimiters = "<>" })),

	s( "||" , {
		t"\\abs{", d(1, get_visual) ,t"} "
	}),

	s( "case" , fmt([[
	\begin{<choice>}
		<case1> & <cond1> \\
		<case2> & <cond2>
	\end{<rep>}

	]], {
		choice = c(1, { t"case", t"dcase"}),
		case1 = i(2, "CASE 1"),
		cond1 = i(3, "BETINGELSE 1"),
		case2 = i(4, "CASE 2"),
		cond2 = i(5, "BETINGELSE 2"),
		rep = rep(1),
	}, { delimiters = "<>" })),

	s( "svar" , fmt("\\begin{svar}\n\t<>\n\\end{svar}", {
		i(0),
	}, { delimiters = "<>" })),

	s( "bm" , fmt("\\bm{<>} ", {
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( "ti" , fmt("\\tilde{<>} ", {
		d(1, get_visual),
	}, { delimiters = "<>" })),

},

{
	-- automatic snippets
	s( "$$" , {
		t"$", d(1, get_visual), t"$ "
	}),

	s( { trig = "=>", wordTrig = false } , {
		t"\\Rightarrow "
	}),

	s( { trig = "->", wordTrig = false } , {
		t"\\rightarrow "
	}),

	s( "\\%[\\" , fmt("\n\n\n\\begin{equation*}\n\t<>\n\\end{equation*}\n\n\n", {
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( { trig = "(", wordTrig = false } , fmt("({}) ", {
		d(1, get_visual),
	})),

	s( { trig = "^", wordTrig = false } , fmt("^{<>} ", {
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( { trig = "_", wordTrig = false } , fmt("_{<>} ", {
		d(1, get_visual),
	}, { delimiters = "<>" })),

	s( { trig = "([%s&])\\iff", regTrig = true, wordTrig = false }, {
		f(function(_,snip) return snip.captures[1] .. "\\Leftrightarrow " end),
	}),

	s( { trig = "([%s%$]%a)(%d)", regTrig = true, wordTrig = false}, {
		f(function(_,snip) return snip.captures[1] .. "_{" .. snip.captures[2] .. "} " end),
	}),

	s( { trig = "(.)%.%.%.", regTrig = true, wordTrig = false}, {
		f(function(_,snip)
			if snip.captures[1] == "," then
				return snip.captures[1] .. "\\ldots "
			else
				return snip.captures[1] .. "\\cdots "
			end
		end),
	}),
}
