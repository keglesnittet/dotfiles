local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local i = ls.insert_node
local t = ls.text_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local extras = require("luasnip.extras")
local n = extras.nonempty
local fmt = require("luasnip.extras.fmt").fmt
local l = require("luasnip.extras").lambda
local dl = extras.dynamic_lambda

-- Summary: When `LS_SELECT_RAW` is populated with a visual selection, the function
-- returns an insert node whose initial text is set to the visual selection.
-- When `LS_SELECT_RAW` is empty, the function simply returns an empty insert node.
local get_visual = function(args, parent)
	if (#parent.snippet.env.LS_SELECT_RAW > 0) then
		return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
	else  -- If LS_SELECT_RAW is empty, return a blank insert node
		return sn(nil, i(1))
	end
end

-- given a path this returns the head/parrent of the path
local function headOfPath(path)
	if string.match(path, "/") then
		return string.reverse(string.sub(string.match(string.reverse(path), "/.*"),2))
	else
		return ""
	end
end

-- returns true if "name" is the path of a file which exists (is readable)
local function file_exists(name)
   local file=io.open(name,"r")
   if file~=nil then io.close(file) return true else return false end
end

local function yaml_exists()
	local path_to_check = vim.fn.expand('%:p')
	while #path_to_check > 0 do
		if file_exists(path_to_check .. "/.info.yml") then
			return true
		end
		path_to_check = headOfPath(path_to_check)
	end
	return false
end

local function yaml_path()
	if yaml_exists() == true then
		local path_to_check = vim.fn.expand('%:p')
		while #path_to_check > 0 do
			if file_exists(path_to_check .. "/.info.yml") then
				return path_to_check .. "/.info.yml"
			end
			path_to_check = headOfPath(path_to_check)
		end
	end
end

local function get_info()
	if yaml_exists() then
		local yaml = require('lyaml')
		local info_file = io.open(yaml_path(), "r")
		local content = info_file:read("*all")
		info_file:close()
		return yaml.load(content)
	else
		return nil
	end
end

local info = get_info()
-- if the yaml file exists you can get the value of e.g. the field "sprog" by info.sprog

return {
	-- non-automatic snippets
	s("start", fmt([[
	input skeie

	beginfig(1);

		{}

	endfig;
	end;
	]],{
		i(0),
	})),

	s("beginfig", fmt([[
	{comment}{before}{name}.mps"
	beginfig({number});

		{visual}

	endfig;
	]], {
		comment = n(1, "", "% "),
		before = f(function()
			if yaml_exists then
				return 'outputtemplate := "' .. info.mpost.out
			end
			return 'outputtemplate := "'
		end),
		name = i(1, "name"),
		number = d(2, function()
			return sn(nil, i(1, tostring(vim.fn.searchcount({ pattern = 'beginfig'}).total )))
		end
		),
		visual = i(0),
	})),

	s( "for", fmt([[
	for {i} = {start} step 1 until {stop}:
		{content}
	endfor
	]], {
		i = i(1, "i"),
		start = i(2, "0"),
		stop = i(3, "end"),
		content = i(0, "content"),
	})),

	s( "vardef", fmt([[
	vardef {name}(expr {arg}) =
	begingroup
		save {content};
	endgroup
	enddef;
	]],{
		name = i(1, "name"),
		arg = i(2, "arg"),
		content = i(0, "content"),
	})),

	s( "dot", fmt('{anchor}(TEX("$${label}$$"),{position});', {
		anchor = c(1, {
			t"dotlabel",
			t"dotlabel.top",
			t"dotlabel.bot",
			t"dotlabel.rt",
			t"dotlabel.lft",
			t"dotlabel.urt",
			t"dotlabel.lrt",
			t"dotlabel.ulft",
			t"dotlabel.llft",
		}),
		label = i(2, "label"),
		position = dl(3, l._1, 2),
	})),

}
