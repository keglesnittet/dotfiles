local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
-- local f = ls.function_node
local t = ls.text_node
local d = ls.dynamic_node
local c = ls.choice_node
local sn = ls.snippet_node
local fmt = require("luasnip.extras.fmt").fmt
local extras = require("luasnip.extras")
local l = extras.lambda
local dl = extras.dynamic_lambda

return {
	s( "info" , fmt([[
titel: '<titel>'
kort_titel: '<kort_titel>'
sprog: '<sprog>'
opgave_nummer: '\textbf{<nummer>}'
aar: '<aar>'
blok: '<blok>'
mpost:
  main: 'Metapost/' #relative to main.tex
  out: '' #relative to main.mp
quiver: '<conf>'
	]], {
		titel = d(1,function()
			local title = string.gsub(vim.fn.expand('%:p:h:t'), "-", " ")
			return
			sn(nil, {i(1, title)})
		end),
		kort_titel = dl(2, l._1:gsub("%s%l", string.upper):gsub("^%l", string.upper), 1),
		sprog = c(3, {i(1, "dansk"), i(2, "engelsk")}),
		nummer = i(4, "\\alph*)"),
		aar = i(5),
		blok = i(6),
		conf = c(7, {t"yes", t"no"}),
	}, { delimiters = "<>" })),

}
