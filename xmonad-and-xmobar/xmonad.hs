import XMonad
import Data.Monoid
import Graphics.X11.ExtraTypes.XF86
import System.Exit
import qualified XMonad.Actions.CycleWS as CWS
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WorkspaceHistory
import XMonad.Layout.Magnifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab
import qualified XMonad.StackSet as W
import qualified Data.Map        as M


main = do
  xmproc <- spawnPipe "xmobar /home/soren/.config/xmobar/xmobar.hs"
  xmonad $ docks $ ewmhFullscreen $ ewmh $ withEasySB (statusBarProp "xmobar" (pure myXmobarPP)) defToggleStrutsKey $ myConfig
  where
    defToggleStrutsKey :: XConfig Layout -> (KeyMask, KeySym)
    defToggleStrutsKey XConfig{ modMask = m } = (m .|. shiftMask, xK_s)

------------------------------------------------------------------------------------------
---------------------------------------- Settings ---------------------------------------- 
------------------------------------------------------------------------------------------

myTerminal = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth   = 2
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#ebdbb2" -- "#dddddd"
myFocusedBorderColor = "#ff0000"

-----------------------------------------------------------------------
-- til at tage screenshots både med et klippeværktøj og uden
-- The command to lock the screen or show the screensaver.
myScreensaver = "/usr/bin/gnome-screensaver-command --lock"

-- The command to take a selective screenshot, where you select
-- what you'd like to capture on the screen.
mySelectScreenshot = "scrot -s -z -F '/home/soren/Pictures/select_screenshots/select_screenshot_from_%d-%m-%Y_%H:%M:%S.png' && notify-send 'Screenshot taken'"

-- The command to take a fullscreen screenshot.
myScreenshot = "scrot -z -F '/home/soren/Pictures/Screenshots/screenshot_from_%d-%m-%Y_%H:%M:%S.png' && notify-send 'Screenshot taken'"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf) -- launch a terminal
    , ((modm .|. shiftMask, xK_p     ), spawn "passmenu -i") -- launch passdmenu
    , ((mod1Mask .|. shiftMask, xK_p ), spawn "sh /home/soren/.config/rofi/scripts/powermenu_t2") -- poweroff
    , ((modm,               xK_f     ), spawn "alacritty --command ranger") -- launch ranger
    , ((modm .|. shiftMask, xK_f     ), spawn "bash /home/soren/.xmonad/launcher/launcher.sh") -- launch launcher
    , ((modm,               xK_c     ), spawn "alacritty --command cmus") -- launch cmus
    , ((modm,               xK_v     ), spawn "cd /home/soren/Documents/Matematik/KU/Aktive_Kurser && alacritty -e ranger") -- åben aktive kurser i ranger
    , ((modm .|. shiftMask, xK_v     ), spawn "bash /home/soren/.xmonad/kurser/xmonad_start_kursus.sh") -- kør kursus.sh
    , ((modm,               xK_a     ), spawn "bash /home/soren/.xmonad/afleveringer/xmonad_start_afleveringer.sh") -- Åben eller lav en ny aflevering
    , ((modm .|. shiftMask, xK_t     ), spawn "bash /home/soren/.xmonad/todo/todo_launcher.sh") -- launch todo
    , ((modm .|. controlMask, xK_t   ), spawn "bash /home/soren/.xmonad/todo/launcher_launcher.sh") -- launch todo/sorter
    , ((modm, xK_p                   ), spawn "sh /home/soren/.config/rofi/scripts/launcher_t4") -- launch gmrun
    , ((modm,               xK_b     ), spawn "torbrowser-launcher") -- launch TorBrowser
    , ((modm .|. shiftMask, xK_b     ), spawn "firefox") -- launch Firefox
    , ((0, xF86XK_AudioRaiseVolume)           , spawn "pamixer -i 5 -u") -- lyd op
    , ((0, xF86XK_AudioLowerVolume)           , spawn "pamixer -d 5 -u") -- lyd ned
    , ((shiftMask, xF86XK_AudioRaiseVolume)   , spawn "pamixer -i 1 -u") -- lyd lidt op
    , ((shiftMask, xF86XK_AudioLowerVolume)   , spawn "pamixer -d 1 -u") -- lyd lidt ned
    , ((0, xF86XK_AudioMute)                  , spawn "pamixer -t") -- Mute
    , ((0, xF86XK_AudioMicMute)               , spawn "pamixer -t --source $(pamixer --list-sources | grep 'input' | grep -Eo '^[0-9]+')") -- Mute Microphone
    , ((0, xF86XK_AudioPlay)                  , spawn "cmus-remote -u") -- toggle play
    , ((0, xF86XK_AudioNext)                  , spawn "cmus-remote -n") -- næste
    , ((0, xF86XK_AudioPrev)                  , spawn "cmus-remote -r") -- forrige
    , ((0, xF86XK_AudioStop)                  , spawn "rfkill toggle bluetooth") -- toggle bluetooth
    , ((0, xF86XK_MonBrightnessUp)            , spawn "light -s sysfs/backlight/amdgpu_bl0 -A 5") -- lys op
    , ((0, xF86XK_MonBrightnessDown)          , spawn "light -s sysfs/backlight/amdgpu_bl0 -U 5") -- lys ned
    , ((shiftMask, xF86XK_MonBrightnessUp)    , spawn "light -s sysfs/backlight/amdgpu_bl0 -A 1") -- lys lidt op
    , ((shiftMask, xF86XK_MonBrightnessDown)  , spawn "light -s sysfs/backlight/amdgpu_bl0 -U 1") -- lys lidt ned
    , ((0, xF86XK_Calculator)                 , spawn myScreenshot)
    , ((shiftMask, xF86XK_Calculator)         , spawn mySelectScreenshot)
    , ((modm, xF86XK_Calculator)              , spawn "alacritty --command R")
    , ((mod1Mask, xK_l)                       , spawn "systemctl suspend; slock")
    , ((0, xK_Num_Lock)                       , spawn "sleep 1; numlockx") -- Gør så din Num_Lock ikke er praktisk. Dette er et bodge - fix det

    , ((modm .|. shiftMask, xK_c     ), kill) -- close focused window
    , ((modm,               xK_space ), sendMessage NextLayout) -- Rotate through the available layout algorithms
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf) --  Reset the layouts on the current workspace to default
    , ((modm,               xK_n     ), refresh) -- Resize viewed windows to the correct size
--    , ((modm,               xK_Tab   ), windows W.focusDown) -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown) -- Move focus to the next window
    , ((modm,               xK_k     ), windows W.focusUp  ) -- Move focus to the previous window
--    , ((modm,               xK_m     ), windows W.focusMaster  ) -- Move focus to the master window
--    , ((modm,               xK_Return), windows W.swapMaster) -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  ) -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    ) -- Swap the focused window with the previous window
    , ((modm,               xK_h     ), sendMessage Shrink) -- Shrink the master area
    , ((modm,               xK_l     ), sendMessage Expand) -- Expand the master area
    , ((modm,               xK_t     ), withFocused $ windows . W.sink) -- Push window back into tiling
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1)) -- Increment the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1))) -- Deincrement the number of windows in the master area

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    
    , ((modm .|. controlMask , xK_l     ), CWS.nextWS)
    , ((modm .|. controlMask , xK_h     ), CWS.prevWS)

    , ((modm                 , xK_g     ), toggleWindowSpacingEnabled)
    , ((modm .|. shiftMask   , xK_s     ), sendMessage $ ToggleStruts)

    -- , ((modm              , xK_g     ), sendMessage $ ToggleGaps)

    -- Quit xmonad
    --, ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "killall xmobar && xmonad --recompile && xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    --, ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--

myLayout = spacingRaw True (Border 0 10 10 10) False (Border 10 10 10 10) False $ avoidStruts (smartBorders (tiled ||| Full))
  where
    -- default tiling algorithm partitions the screen into two panes
    tiled   = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100


------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "minlauncher"    --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]
--fullscreenManageHook
------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = workspaceHistoryHook --return () --dynamicLogWithPP myXmobarPP

myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = " | "
--    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = purple . wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
    , ppHidden          = blue . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
--    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \(ws : l : _ : _ ) -> [l,ws]
--    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
--    , ppTitle           = const ""
--    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 10

    blue, purple, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#02daf7" ""
    purple   = xmobarColor "#f702f3" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""
------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
    spawn "picom --config ~/.config/picom/picom.conf &"
--  spawn "picom --experimental-backends --config ~/.config/picom/picom.conf &"
    spawn "nitrogen --restore &"
    spawn "xmobar /home/soren/.config/xmobar/xmobar.hs &"
    spawn "/home/soren/.xmonad/xmonadprofile.sh"
    spawn "rfkill block bluetooth"

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
--  xmonad $ docks $ ewmhFullscreen $ ewmh $ xmobarProp $ myConfig

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
myConfig = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
