Config { font = "xft:DejaVu Sans:pixelsize=14:antialias=True:hinting=True"
    , additionalFonts = ["xft:Mononoki Nerd:pixelsize=12:antialias=True:hinting=True"]
    , borderColor = "black"
    , border = TopB
    , bgColor = "black"
    , fgColor = "#ebdbb2"
    , alpha = 200
    , position = TopH 21
    , textOffset = 0
    , iconOffset = -1
    , lowerOnStart = True
    , pickBroadest = False
    , persistent = False
    , hideOnStart = False
    , iconRoot = "."
    , allDesktops = True
    , overrideRedirect = True
    , commands = [ Run DateZone "%a %_d. %b %Y  %H:%M" "da_DK.utf8" "Europe/Copenhagen" "date" 600
        , Run BatteryP ["BAT0"] ["-t", "Batt: <leftipat>", 
                                 "-L", "20", "-p", "3",
                                 "-l", "red",
                                 "--", 
                                 "--on-icon-pattern", "<fc=#42dafa><left>%</fc>",
                                 "--off-icon-pattern", "<left>%"]
                                 10
        , Run Brightness ["-t", "Lys: <percent>%", "--", "-D", "amdgpu_bl0"] 50
        , Run Com "/home/soren/.config/xmobar/lyd/lyd.sh" [] "lyd" 10
        , Run Com "/home/soren/.config/xmobar/lyd/bluetooth.sh" [] "blue" 600
        , Run Com "/home/soren/.config/xmobar/lyd/mic.sh" [] "mic" 600
        , Run Mpris2 "cmus" ["-t", "<artist> - <title>", "--maxtwidth", "55"] 60
        , Run Cpu ["-L","3","-H","50", "--normal","green","--high","red"] 10
        , Run Memory ["-t","Mem: <usedratio>%"] 10
        , Run XMonadLog
    ]
    , sepChar = "%"
    , alignSep = "{}"
    , template = "%lyd% | %mic% | %bright% | %cpu% | %memory% | BT: %blue%{<fc=#ee9a00>%date%</fc>}%mpris2% | %XMonadLog% | %battery%  "
}
