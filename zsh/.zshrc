# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/soren/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Enable colors and change prompt:
autoload -U colors && colors

setopt prompt_subst
autoload -Uz vcs_info # enable vcs_info
precmd () { vcs_info } # always load before displaying the prompt

# List of formats for $vcs_info_msg_0_ and $vcs_info_msg_1_ resp.
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' formats '%F{#8ec07c}%b%f ' '%s(%F{#8ec07c}%b%f%F{#d79921}%c%f%F{#fb4934}%u%f)'
zstyle ':vcs_info:*' unstagedstr ' ✗'
zstyle ':vcs_info:*' stagedstr ' ✓'

# unstaged = "✗",
# staged = "✓",
# unmerged = "",
# renamed = "➜",
# untracked = "★",
# deleted = "",
# ignored = "◌",

# Setup promt
_setup_ps1() {
  vcs_info
  PS1='%F{#fb4934}┌───(%f%F{#fabd2f}%B%n%b%f%F{#fb4934})─[ %f%F{#83a598}%~%f%F{#fb4934} ]%f
%F{#fb4934}└─ λ $vcs_info_msg_0_%f$(_setup_ps1)'
  RPROMPT='$vcs_info_msg_1_$(_setup_ps1)'
}
_setup_ps1
FORCE_RUN_VCS_INFO=1

# Basic auto/tab complete:
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'Underline' ]]; then
    echo -ne '\e[4 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[4 q"
}
zle -N zle-line-init
echo -ne '\e[4 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[4 q' ;} # Use beam shape cursor for each new prompt.

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Use FZF to look though history by using ^r (ctrl + r)
zle -N my-script_widget
bindkey '^r' my-script_widget
my-script_widget() LBUFFER+=$(cat /home/soren/.zsh_histfile | fzf)

# Load aliases (with functions) and settings
source ~/.config/zsh/aliases.zsh
source ~/.config/zsh/settings.zsh

# Load Plugins
# get them by installing the packages: zsh-syntax-highlighting and zsh-autosuggestions
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
