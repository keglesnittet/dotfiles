# Dotfiles

This is a collection of some of my dotfiles for my Arch (btw) setup. This repository will almost definitely *not* stay up-to-date. These dotfiles are meant purely to give inspiration; they are not plug-and-play and are not tested thoroughly. Also many of the comments are in Danish. If something does not work, feel free to contact me (contact information can be found on my website).

Some of the configs use scripts I have written myself, those are not in this repository.

## List of programs

Here is a list of the programs, for which my dotfiles are in this repository:

* Alacritty
* Hyprland + Hyprpaper + Waybar
* NVim
* XMonad + XMobar
* ZSH
