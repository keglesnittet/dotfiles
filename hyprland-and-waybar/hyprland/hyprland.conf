# hyprpaper config and waybar
# hyprpaper preload /home/soren/.config/hypr/background/cowboy.png
exec-once = hyprpaper & waybar 

env = XCURSOR_SIZE,24
monitor = eDP-1, 1920x1080@60, 0x0,1
monitor = HDMI-A-1,1920x1080@60, auto,1,transform,1
monitor=,preferred,auto,1 # for when plugging in new monitors

xwayland {
	force_zero_scaling = true
}

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = dk
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =
	numlock_by_default = true

    follow_mouse = 1

    touchpad {
        natural_scroll = yes
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    gaps_in = 0
    gaps_out = 0
    border_size = 3
    col.active_border   = rgb(cc241d)
    col.inactive_border = rgb(ebdbb2)

    layout = master
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 0
    
    blur {
        enabled = true
        size = 3
        passes = 2
    }

	# inactive_opacity = 0.8
    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = no
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
	mfact = .5
	no_gaps_when_only = 1
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind  = $mainMod SHIFT, C, killactive, 
bind  = $mainMod, Q, exec, killall waybar && waybar
bind  = $mainMod, T, togglefloating, 
bind  = $mainMod SHIFT, RETURN, exec, alacritty
bind  = $mainMod, F, exec, alacritty -e ranger

# Move focus with mainMod + arrow keys
binde = $mainMod, K, layoutmsg, cycleprev
binde = $mainMod, J, layoutmsg, cyclenext
bind  = $mainMod SHIFT, K, layoutmsg, swapprev
bind  = $mainMod SHIFT, J, layoutmsg, swapnext

bind = $mainMod, SPACE, fullscreen, 1

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
binde = $mainMod CTRL, L, workspace, r+1
binde = $mainMod CTRL, H, workspace, r-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow


bind  = $mainMod SHIFT, P, exec, passmenu -i
bind  = ALT SHIFT, P, exec, sh /home/soren/.config/rofi/scripts/powermenu_t2
bind  = ALT SHIFT, L, exec, waylock -fork-on-lock -fail-color 0xfb4934 -input-color 0x076678
bind  = $mainMod SHIFT, F, exec, [float] bash /home/soren/.xmonad/launcher/launcher.sh
bind  = $mainMod, C, exec, alacritty --command cmus
bind  = $mainMod, V, exec, cd /home/soren/Documents/Matematik/KU/Aktive_Kurser && alacritty -e ranger
bind  = $mainMod SHIFT, V, exec, cd /home/soren/Documents/Matematik/KU/Aktive_Kurser/Knot-Theory\(PUK\)/knot-theory-puk/ && alacritty -e nvim main.tex
bind  = $mainMod, A, exec, [float] bash /home/soren/.xmonad/afleveringer/xmonad_start_afleveringer.sh
bind  = $mainMod, P, exec, sh /home/soren/.config/rofi/scripts/launcher_t4
bind  = $mainMod, B, exec, torbrowser-launcher
bind  = $mainMod SHIFT, B, exec, firefox
bind  = ALT, Q, exec, firefox --private-window 'q.uiver.app'

binde = , XF86AudioRaiseVolume, exec, pamixer -i 5 -u
binde = , XF86AudiolowerVolume, exec, pamixer -d 5 -u
binde = SHIFT, XF86AudioRaiseVolume, exec, pamixer -i 1 -u
binde = SHIFT, XF86AudiolowerVolume, exec, pamixer -d 1 -u
bindl = , XF86AudioMute, exec, pamixer -t
bindl = , XF86AudioMicMute, exec, pamixer -t --source $(pamixer --list-sources | grep 'input' | grep -Eo '^[0-9]+')
bindl = , XF86AudioPlay          , exec, cmus-remote -u
bindl = , XF86AudioNext          , exec, cmus-remote -n
bindl = , XF86AudioPrev          , exec, cmus-remote -r
bindl = , XF86AudioStop          , exec, rfkill toggle bluetooth
binde = , XF86MonBrightnessUp    , exec, brightnessctl set +5%
binde = , XF86MonBrightnessDown  , exec, brightnessctl set 5%-
binde = SHIFT, XF86MonBrightnessUp    , exec, brightnessctl set +1%
binde = SHIFT, XF86MonBrightnessDown  , exec, brightnessctl set 1%-

bind = , XF86Calculator, exec, sh /home/soren/.config/hypr/scripts/screenshot.sh
bind = SHIFT, XF86Calculator, exec, sh /home/soren/.config/hypr/scripts/select-screenshot.sh
bind  = $mainMod, XF86Calculator, exec, alacritty --command R
